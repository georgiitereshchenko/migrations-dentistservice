﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Migrations_DentistService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService
{
    public class MigrationDbContext : DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Visits> Visits { get; set; }
        public DbSet<DentalCard> DentalCards { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Teeth> Teeths { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var bilder = new ConfigurationBuilder();
            bilder.SetBasePath(AppDomain.CurrentDomain.BaseDirectory);
            bilder.AddJsonFile("appsetings.json");
            var config = bilder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
