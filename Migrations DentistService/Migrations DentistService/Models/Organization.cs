﻿using Migrations_DentistService.Interfase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService.Models
{
    public class Organization : IEtity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Supervisor { get; set; }
    }
}
