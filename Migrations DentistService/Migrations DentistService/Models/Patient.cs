﻿using Migrations_DentistService.Interfase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Migrations_DentistService.Models
{
    public class Patient : IEtity<int>
    {
        public int Id { get; set;}
        public string FirstName { get; set;}
        public string LastName { get; set;}
        public string Patronymic { get; set;}
        public DateTime DateOfBirst { get; set;}
        public byte Sex { get; set;}
        public DateTime AccountCreationDate { get; set;}
        [ForeignKey("Organization")]
        public int OrganizationId { get; set;}
        public virtual Organization Organization { get; set; }


    }
}
