﻿using Migrations_DentistService.Interfase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService.Models
{
    public class Teeth : IEtity<int>
    {
        public int Id { get; set; }
        public int JawType { get; set; }
        public int JawSide { get; set; }
        public int ToothNomer { get; set; }
        
    }
}
