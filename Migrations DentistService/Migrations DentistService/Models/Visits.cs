﻿using Migrations_DentistService.Enums;
using Migrations_DentistService.Interfase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Migrations_DentistService.Models
{
    public class Visits : IEtity<int>
    {
        public int Id { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime DateVisit { get; set; }

        public decimal? Price { get; set; }
        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        [ForeignKey("User")]
        public  int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}
