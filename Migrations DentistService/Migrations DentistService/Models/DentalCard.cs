﻿using Migrations_DentistService.Interfase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Migrations_DentistService.Models
{
    public class DentalCard : IEtity<int>
    {
        public int Id { get; set; }
        public string Info { get; set; }
        public string Comment { get; set; }

        [ForeignKey("Visits")]
        public int VisitId { get; set; }
        public virtual Visits Visits { get; set; }

        [ForeignKey("Teeth")]
        public int TeethId { get; set; }
        public virtual Teeth Teeth { get; set; }
    }
}
