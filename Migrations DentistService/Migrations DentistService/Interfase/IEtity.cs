﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService.Interfase
{
    public interface IEtity<T>
    {
        public T Id { get; set; }
    }
}
