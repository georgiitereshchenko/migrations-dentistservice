﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService.Enums
{
    public  enum VisitType
    {
        PullOutTooth = 1,
        FixTooth = 2,
        Inspection =3,
    }
}
