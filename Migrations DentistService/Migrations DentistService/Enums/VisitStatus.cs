﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations_DentistService.Enums
{
    public enum VisitStatus
    {
        Appointed = 1,
        AtTheReception =2,
        Finished =3,
        Overdue = 4,
    }
}
