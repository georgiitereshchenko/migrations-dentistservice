﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations_DentistService.Migrations
{
    public partial class reneme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DentalCards_Visits_VisitsnId",
                table: "DentalCards");

            migrationBuilder.DropIndex(
                name: "IX_DentalCards_VisitsnId",
                table: "DentalCards");

            migrationBuilder.DropColumn(
                name: "VisitsnId",
                table: "DentalCards");

            migrationBuilder.AlterColumn<int>(
                name: "JawType",
                table: "Teeths",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JawSide",
                table: "Teeths",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VisitId",
                table: "DentalCards",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DentalCards_VisitId",
                table: "DentalCards",
                column: "VisitId");

            migrationBuilder.AddForeignKey(
                name: "FK_DentalCards_Visits_VisitId",
                table: "DentalCards",
                column: "VisitId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DentalCards_Visits_VisitId",
                table: "DentalCards");

            migrationBuilder.DropIndex(
                name: "IX_DentalCards_VisitId",
                table: "DentalCards");

            migrationBuilder.DropColumn(
                name: "VisitId",
                table: "DentalCards");

            migrationBuilder.AlterColumn<string>(
                name: "JawType",
                table: "Teeths",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "JawSide",
                table: "Teeths",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "VisitsnId",
                table: "DentalCards",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DentalCards_VisitsnId",
                table: "DentalCards",
                column: "VisitsnId");

            migrationBuilder.AddForeignKey(
                name: "FK_DentalCards_Visits_VisitsnId",
                table: "DentalCards",
                column: "VisitsnId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
